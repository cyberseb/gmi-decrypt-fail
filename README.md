# gmi-decrypt-fail

This repo includes the source code of gemini://tx.decrypt.fail

Gemini is a new, collaboratively designed internet protocol, which explores the space inbetween gopher and the web, striving to address (perceived) limitations of one while avoiding the (undeniable) pitfalls of the other.

You can learn more about Gemini here: https://gemini.circumlunar.space

To experience gemini://tx.decrypt.fail you can either get yourself a Gemini client (https://gemini.circumlunar.space/clients.html) or use a web2gemini proxy (https://portal.mozz.us/gemini/tx.decrypt.fail/).
```
     _____                                       _____                               
  __|___  |__ __    _ ______  ______  _____   __|__   |__  __   _  ____   _  __  __  
 |   ___|    |\ \  //|      >|   ___||     | |     |     ||  | | ||    \ | ||  |/ /  
 |   |__     | \ \// |     < |   ___||     \ |    _|     ||  |_| ||     \| ||     \  
 |______|  __| /__/  |______>|______||__|\__\|___|     __||______||__/\____||__|\__\ 
    |_____|                                     |_____|                              
                
```
"Gibson said it in a short story somewhere. Cyberpunk is the stuff that has EDGE written all over it. You know, not edge, it's written EDGE. All capital letters. Now ask me how I'd define EDGE. Well, EDGE is not about definitions. To the contrary, things so well known that they provide an exact definition can't be EDGE. They probably once were but now they ain't. SO DON'T TRY TO DEFINE IT!!!"
~ Thomas Eicher

```
                        _______
            _,.--''''''  -    ''`-...__
          /''___...---    -   ---..    `.._
        .,--'              `-  __  `-.     .
      .'/     ___..--------  -   ''---'-.   `.
      /J  ,-''         __,.   -   .._  `-'-. `\
    ,//  |     _..--'''    _         '-._|  `  \
   / |  ,'  ,-'      _..--'    -  _____ `\|  '  `.
 ,'  /    ,'  ,  ,.-'   _.-            '-'|    | \
 '  / .','   / ,-    _,'  :  :      -.    '.|  |  \
   |  /'    //'    /'   . /  | \  \   \    \+   |  |
   | //   .'/     |     |.'  | |   |  '.    +\  |  \
   ||/    J|      /    / |   | '.  '.  |    '.\ \   \
   |||   | |     |   ,'  |   |  \   \   |   || ||   |
   ||    J J    ,'   |  /_   |  |   |  ,|    |   |  |
   ||   | |    .'|. .||/'|   | / |  + |\'     |  |  |
   ||   | |  . | \ ./ |,'|/'|'/\  _'|,'| /  / |  |  L
   ||   | | |'.| |'--..._`  | /| ,' |'_,.. -  || |   |
   /L   | / | \|,.....  -'\ |/ |/  . .....    |\ |  .'
   | |  | | |   `._(  )\           /(  )_.'   |  |  /
  | .\  J | \      '''''           '````     /'   .|
  |  | |  |  |                              .|  ,.'|
  |  |.'. | _\.                             /  / | |
  || || |  \'\:                            /   | / |
   +L||-\...__ -._                        / /  | | |
 | '||.+ \ _  '`-.:._     _     _        /'|   J|  J
 |  \\ +` \ -.     `       `...'        ,' |  | | |
 |  || |.  .         _ _               /'  |  | | |
 |  '.  |   \          \''------;'   ,'.'  |  | | |
  |  || |\   `._        '-.._..'   ,/  |   |  / | L
  \  ||  |\     '-.               ,|'' |   |  | | '|
   | '.| \ `\      `             J ||  |   |  | |  |
   \  ||  |  \       '..      ,-'  /|  |   |  | |  L.
   '. ||  `    -.       '''`-' |   '|  |   /  | |   |
    | | |  '-..  `.            '''-'\ ._....._| |   |
    | | '.         `                ||_ `.     ' . |
   /+    \                         /'|'`. \        `.
   \_'                           ,-,/.   \ \         \
  \,,' _--..__                _,/,/   `   \ \         \
  \'\  ||  ..__'--......___./, -,  ``      \ \         .
   \ \ ''   ||`'+--..... . -'\\  \\        | |         |
    \ \     ||' ||   ||  \\   \\  "        \ \         |
     \ \        ||   ||   \\   "            \ \        '
     \  \                                    \ \      /
      \  .                                    \ \   |/
       | .                                     \ \  |
       |  .                                    '  \ |
       \  .                                     \  \|
        \  \                                     \  `.
         |  \                                     \   \
         |   \                                     \   `.
         '    \                                     Y    \
          |    \               .                    |     \
          |     '               `                   J      `
          |      `.              `                 /         \
         J         `.             `               /           '
         |           `._           `             '             `
         |              '-._        `           .              :
        J                   `-.._    .         /
        |                        -.  :        /                '
        |                          `\:      .                ,'
        J                            Y    ,'                '
       /                             |-._/                '
      /                              |  /          _,_,.'
     .'                              | Y       , -' '  |
     J                               | | ,..--'        |
   _-.                               /.-             _/
    |  -.._                    _, -'''             ,'/
    |     '-...._________,.-''                 ,-'  /
    |                                    __,,-'     |\
    |_                             __.-.''          | \
     '`-..___            _____, -''  /'\            |  \
     \       '''-------''           /   \           |   \
      \                            /     \       _.'|    \
       |                          /       \  _.-'   |     \
       |                         /         -'       |      \
       |`-._                    /                   \       \
       |    '--..____,......---'                     \       \
     ,'                                               \       .
   ,'                                                 /\       .
 .'                                                  /  \       .
'  \                                                /    \
    \                                  ( )         /      \
     \                                            /        \
      \                                          /    
```

Cyberpunk went in and out of media attention over the last couple of decades. It is a term losely defined but capable of spurring many ways of life as well as interesting stories.
On this page I have collected some interesting texts about CP. Most of them I have copied from the fabulous Cyberpunk Project:
=> https://project.cyberpunk.ru Project Cyberpunk

Articles explaining aspects of Cyberpunk:
=> manifesto.gmi A Cyberpunk Manifesto
=> why-cyberpunk.gmi Why Cyberpunk?
=> alt.cyberpunk.gmi alt.cyberpunk faq
=> social-political-theory.gmi Cyberpunk as Social and Political Theory
=> time-has-come.gmi Time has come

Look up some words in the Dictionary!
=> dictionary.gmi Cyberpunk Dictionary

--
CC-BY-SA-4.0
=> https://codeberg.org/cyberseb/gmi-decrypt-fail source